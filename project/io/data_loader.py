import csv
import os
import pandas as pd

dir = os.path.dirname(__file__)



def load_interest_rates(filename, frequencyCode, dataColumnName, headerLength=6):
  filepath = os.path.join(dir, '../data', filename) + ".csv"
  df = pd.read_csv(filepath, sep=',', parse_dates=[0], header=headerLength, names=['date', dataColumnName], index_col='date')
  df = df.convert_objects(convert_numeric=True)
  df = df.resample(frequencyCode, how='mean')
  df = df.fillna(method='backfill')
  return df