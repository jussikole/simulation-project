import sys
from datetime import datetime
import matplotlib.pyplot as plt
sys.path.append('../')
from ..finance.market import Market
from ..finance.bond import Resolution, Maturity

def main():
  market = Market('treasury_nominal', Maturity(0,1), Resolution.DAILY)
  market.load()
  print market.advance(10)
  print market.advance(10)
  
  print market.date()
  print market.rate()
  
  
  print market.rates.plot()
      
if __name__ == "__main__":
  main()