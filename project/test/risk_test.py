from ..finance.market import RealMarket, Resolution
from ..finance.simulated_market import SimulatedMarket
from ..finance.simulation import Simulation, Strategy
from ..finance.bond import Maturity
from ..models.model import NormalDistributionModel, RevertingNormalModel, ConstantModel
from ..plot.plotter import plot_interest_rates, plot_results
#import cProfile, pstats

class main():
  #model = NormalDistributionModel()
  #model = RevertingNormalModel(10000)
  model = ConstantModel()

  name = 'treasury_nominal'
  maturity = Maturity(10)
  resolution = Resolution.DAILY
  paymentDates = [[1,1], [1,7]]
  
  realMarket = RealMarket(name, maturity, resolution, paymentDates)
  simulatedMarket = SimulatedMarket(model)
  realMarket.start()
  simulatedMarket.start(realMarket)

  ESIMATION_PERIOD_START = 1982
  ESTIMATION_PERIOD_LENGTH = 10 # years
  SIMULATION_LENGTH = 10 # years
  #REPETITIONS = 1000
  REPETITIONS = 1

  realMarket.advance((ESIMATION_PERIOD_START-1962)*2 - 1)
  print("estimated from: " + str(realMarket.currentDate()))

  estimationRates = realMarket.getPeriodRates(ESTIMATION_PERIOD_LENGTH * realMarket.paymentsPerYear)
  simulatedMarket.estimate(estimationRates)
  print(model)

  #realMarket.advance(ESTIMATION_PERIOD_LENGTH * realMarket.paymentsPerYear)
  dates = realMarket.getProjectionDates(SIMULATION_LENGTH * 2)
  print("simulated from: " + str(dates[0]))
  finalRates = []

  results = []
  for i in range(0, REPETITIONS):
    simulation = Strategy(simulatedMarket)
    results.append(simulation.run(dates))
    finalRates.append(simulation.market.rates.tail(1).iloc[0].simulated_rate)


  real = Strategy(realMarket)
  realReturn, rates = real.run(dates)
  simulatedReturns = [x[0] for x in results]
  simulatedRates = [x[1] for x in results]

  print("Return from real market: " + str(realReturn))
  #print("Average return from simulation: " + str(sum(simulatedReturns)/len(simulatedReturns)))

  #plot_interest_rates(realMarket.rates, [])
  #plot_interest_rates(realMarket.rates, [x[1] for x in results])
  #plot_results(simulatedReturns)
  #plot_results(finalRates, binCount=int(100), xlabel="Rate")
