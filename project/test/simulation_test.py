from ..finance.market import RealMarket, Resolution
from ..finance.simulated_market import SimulatedMarket
from ..finance.simulation import Simulation
from ..finance.bond import Maturity
from ..models.model import NormalDistributionModel

def main():
  model = NormalDistributionModel(3)
  
  name = 'treasury_nominal'
  maturity = Maturity(10)
  resolution = Resolution.DAILY
  paymentDates = [[1,1], [1,7]]
  
  realMarket = RealMarket(name, maturity, resolution, paymentDates)
  simulatedMarket = SimulatedMarket(model)
  
  
  simulation = Simulation(realMarket, simulatedMarket)
  simulation.run()
      
if __name__ == "__main__":
  main()