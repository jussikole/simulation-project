from ..finance.market import RealMarket, Resolution
from ..finance.simulated_market import SimulatedMarket
from ..finance.simulation import Simulation, Strategy
from ..finance.bond import Maturity
from ..models.model import NormalDistributionModel, ARMAModel
from ..plot.plotter import plot_interest_rates
#import cProfile, pstats

class main():
  model = ARMAModel((1,1))

  name = 'treasury_nominal'
  maturity = Maturity(10)
  resolution = Resolution.DAILY
  paymentDates = [[1,1], [1,7]]
  
  realMarket = RealMarket(name, maturity, resolution, paymentDates)
  simulatedMarket = SimulatedMarket(model)
  realMarket.start()
  simulatedMarket.start(realMarket)

  ESIMATION_PERIOD_START = 1962
  ESTIMATION_PERIOD_LENGTH = 5 # years
  SIMULATION_LENGTH = 10 # years
  REPETITIONS = 100

  print("estimated from: " + str(realMarket.currentDate()))
  realMarket.advance(ESTIMATION_PERIOD_LENGTH * realMarket.paymentsPerYear - 1)

  estimationRates = realMarket.getPeriodRates(ESTIMATION_PERIOD_LENGTH * realMarket.paymentsPerYear)
  simulatedMarket.estimate(estimationRates)
  print(model)

  dates = realMarket.getProjectionDates(SIMULATION_LENGTH * 2)
  print("simulated from: " + str(dates[0]))

  results = []
  for i in range(0, REPETITIONS):
    simulation = Strategy(simulatedMarket)
    results.append(simulation.run(dates))

  plot_interest_rates(realMarket.rates, [x[1] for x in results])
