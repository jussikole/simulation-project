
class Bond:
  counter = 1
  
  def __init__(self, issuedDate, price, principal, couponRate, maturity, paymentsPerYear):
    self.id = Bond.counter
    Bond.counter += 1
    self.price = price
    self.issuedDate = issuedDate
    self.principal = principal
    self.couponRate = couponRate / 100.0
    self.maturity = maturity
    self.paymentsPerYear = paymentsPerYear   
    self.coupon = principal * self.couponRate
    self.payments = maturity.payments(paymentsPerYear)
    self.paymentsMade = 0
    
  def paymentsLeft(self):
    return self.payments - self.paymentsMade
    
  def collect(self):
    if self.paymentsMade < self.payments - 1:
      self.paymentsMade += 1
      return self.coupon
    elif self.paymentsMade == self.payments - 1:
      self.paymentsMade += 1
      return self.coupon + self.principal
    else:
      return 0
      
  def __str__(self):
    return "Bond #{0}:\t[${1:.2f} on {2}] - [C ${3:.2f} {4}/{5}] - [P ${5:.2f}]".format(self.id, self.price, self.issuedDate.strftime("%Y-%m-%d"), self.coupon, self.paymentsMade, self.payments, self.principal)
      
  def currentPrice(self, currentRate):
    value = 0
    for j in range(1, self.payments - self.paymentsMade + 1):
       value = value + self.coupon / pow(currentRate + 1, j)
       
    value = value + self.principal / pow(currentRate + 1, self.payments - self.paymentsMade)
    return value
        
class Maturity:
  def __init__(self, years, months=0):
    self.years = years
    self.months = months
    self.id = self.createId()
    
  def createId(self):
    str = ""
    if self.years > 0:
      str += "{0}y".format(self.years)
    if self.months > 0:
      str += "{0}m".format(self.months)
    return str
      
  def totalMonths(self):
    return self.years * 12 + self.months
    
  def payments(self, paymentsPerYear):
    return self.totalMonths() / (12 / paymentsPerYear)
  
