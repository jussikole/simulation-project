from datetime import datetime
from portfolio import Portfolio
from matplotlib import pylab
from ..plot.plotter import plot_interest_rates
from ..finance.simulated_market import SimulatedMarket

class Simulation:
  def __init__(self, realMarket, simulatedMarket, startingMoney=1000):
    self.realMarket = realMarket
    self.simulatedMarket = simulatedMarket
    self.startingMoney = startingMoney

  def run(self, estimationPeriods=10):
    self.realMarket.start()  
    self.simulatedMarket.start(self.realMarket)
 
    period = 0
    while self.realMarket.hasMore():
      if period == 0 or originalBond.paymentsLeft() == 0:
        money = self.startingMoney
        portfolio = Portfolio(money)
        originalBond = self.realMarket.buyPrimary(money)
        portfolio.add(originalBond)
        estimationRates = self.realMarket.getPeriodRates(estimationPeriods)
        self.simulatedMarket.estimate(estimationRates)

      dates = self.realMarket.getProjectionDates(1)
      self.simulatedMarket.project(dates)
      
      # Collect coupons
      money = portfolio.collect(self.realMarket.currentDate())
      
      # Buy a new bond from the secondary markets  
      if money > 0 and originalBond.paymentsLeft() > 0:
        bond = self.simulatedMarket.buySecondary(money, originalBond)
        if bond:
          portfolio.add(bond)
        
      self.realMarket.advance()
      period += 1
    
    plot_interest_rates(self.realMarket.rates, self.simulatedMarket.rates)
      
    
class Strategy:
  def __init__(self, market, debug=False):
    self.market = market
    self.debug = debug

  def run(self, dates):
    if isinstance(self.market, SimulatedMarket):
      self.market.project(dates)

    money = 1000
    portfolio = Portfolio(money)
    originalBond = self.market.buyPrimary(money, dates[0])
    portfolio.add(originalBond)

    for date in self.market.paymentDates:
      money = portfolio.collect(date)
      if self.debug:
        print(str(date) + " collected " + str(money))
      # Buy a new bond from the secondary markets  
      if money > 0 and originalBond.paymentsLeft() > 0:
        bond = self.market.buySecondary(date, money, originalBond)
        if bond:
          portfolio.add(bond)
          if self.debug:
            print(str(date) + " bought " + str(bond.principal) + " with price " + str(bond.price))

    return money, self.market.rates

    