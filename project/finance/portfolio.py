class Portfolio:
  def __init__(self, money):
    self.bonds = []
    self.log = []
    self.money = money
    self.startMoney = money
    
  def add(self, bond):
    self.bonds.append(bond)
    self.money -= bond.price
     
  def collect(self, date):
    for bond in self.bonds:
      self.money += bond.collect()
    self.log.append((date, self.money, len(self.bonds)))
    return self.money
    
  def __str__(self):
    #text = ""
    #for row in self.log:
    #  text += "{0} - ${1:.2f} - {2} bonds\n".format(row[0].strftime("%Y-%m-%d"), row[1], row[2])
    #return text
    return "start ${0:.2f} - end ${1:.2f}".format(self.startMoney, self.money)
    
  def plot(self):
    pass