import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from ..io.data_loader import load_interest_rates
from bond import Bond
from market import Market
import cProfile, pstats, StringIO

class SimulatedMarket(Market):
  def __init__(self, model):
    self.model = model
    self.currentDate = None
    
  def start(self, realMarket, lastDate=None):
    lastDate = lastDate or realMarket.getLastDate()
    self.maturity = realMarket.maturity
    self.paymentsPerYear = realMarket.paymentsPerYear
    self.paymentDays = realMarket.paymentDays
    self.frequencyCode = realMarket.getFrequencyCode()
    self.rates = self.createDataFrame(realMarket.currentDate(), lastDate, self.frequencyCode)
    
  def createDataFrame(self, firstDate, lastDate, frequencyCode='D', values=None):
    dates = pd.date_range(firstDate, lastDate, freq=frequencyCode)
    if values == None:
      values = [None]*len(dates)
    data = [dates, values]
    df = pd.DataFrame(zip(*data), columns=['date', 'simulated_rate'])
    df = df.convert_objects(convert_numeric=True)
    df = df.set_index(['date'])
    return df
    
  def estimate(self, rates):
    self.currentDate = rates.tail(1).ix[0].name + timedelta(days=1)
    self.model.estimate(rates)
    
  def project(self, dates):
    previous = None
    projections = self.model.project(dates)

    self.rates = self.createDataFrame(dates[0], dates[-1], self.frequencyCode, projections)
    self.createPaymentDates()
    
  def getCurrentDate(self):
    return self.rates.iloc[0]

  def getCurrentRate(self):
    return self.getCurrentDate().simulated_rate

  def getRate(self, date):
    return self.rates.loc[date].simulated_rate

  def getTotalPeriods(self):
    return self.maturity.payments(self.paymentsPerYear)
    