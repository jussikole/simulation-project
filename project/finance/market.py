import pandas as pd
from datetime import datetime, timedelta
from ..io.data_loader import load_interest_rates
from bond import Bond

class Resolution:
  DAILY     = 'daily'
  WEEKLY    = 'weekly'
  MONTHLY   = 'monthly'
  ANNUAL    = 'annual'

class Market:
  def getFirstDate(self):
    return self.rates.ix[0].name
    
  def getLastDate(self):
    return self.rates.tail(1).ix[0].name
  
  def buyPrimary(self, money, date):
    rate = self.getRate(date)
    return Bond(date, money, money, rate, self.maturity, self.paymentsPerYear)
    
  def buySecondary(self, date, money, originalBond):
    rate = self.getRate(date)
    currentPrice = originalBond.currentPrice(rate)
    price = money
    principal = currentPrice / originalBond.price * originalBond.principal
    bond = Bond(self.currentDate, price, principal, rate, originalBond.maturity, originalBond.paymentsPerYear)
    bond.i = originalBond.paymentsMade
    return bond

  def getCurrentRate(self):
    return self.rates.loc[self.currentDate()].real_rate

  def createPaymentDates(self):
    paymentDates = []
    for year in range(self.getFirstDate().year, self.getLastDate().year+1):
      for dateParts in self.paymentDays:
        dt = datetime(year, dateParts[1], dateParts[0])
        if dt < self.getFirstDate():
          continue
        elif dt <= self.getLastDate():
          paymentDates.append(dt)
        else:
          paymentDates.append(self.getLastDate())
          break
    self.paymentDates = paymentDates

class RealMarket(Market):
  def __init__(self, name, maturity, resolution, paymentDays):
    self.name = name
    self.resolution = resolution
    self.rates = None
    self.maturity = maturity
    self.paymentsPerYear = len(paymentDays)
    self.paymentDays = paymentDays
    self.paymentIndex = 0
    
  def filename(self):
    return "{0}_{1}_{2}".format(self.name, self.maturity.id, self.resolution)
    
  def loadFromFile(self):
    self.rates = load_interest_rates(self.filename(), self.getFrequencyCode(), 'real_rate')
    
  def getFrequencyCode(self):
    if self.resolution == Resolution.DAILY:
      return 'D'
    elif self.resolution == Resolution.WEEKLY:
      return 'W'
    elif self.resolution == Resolution.MONTHLY:
      return 'M'
    elif self.resolution == Resolution.ANNUAL:
      return 'Y'
    else:
      return None
      
  def start(self):
    self.loadFromFile()
    self.createPaymentDates()
    return self.currentDate()
    
  def getPeriodRates(self, n=1):
    if self.paymentIndex == 0:
      start = self.getFirstDate()
    else:
      n = min(self.paymentIndex, n)
      start = self.paymentDates[self.paymentIndex-n]
    
    end = self.currentDate()
    return self.rates[start:end]
    
  def currentDate(self):
    return self.paymentDates[self.paymentIndex]

  def getProjectionDates(self, n=1):
    n = min(len(self.paymentDates)-self.paymentIndex-1, n)
    start = self.currentDate() + timedelta(days=1)
    end = self.paymentDates[self.paymentIndex+n]
    return pd.date_range(start, end, freq=self.getFrequencyCode())
    
  def hasMore(self):
    return self.paymentIndex < len(self.paymentDates) - 1
    
  def advance(self, n=1):
    self.paymentIndex += n
    self.previousDate = self.currentDate() + timedelta(days=1)
    return self.getPeriodRates()
    
  def getRate(self, date):
    return self.rates.loc[date].real_rate

    
    
    
    