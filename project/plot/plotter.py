from matplotlib import pylab
import numpy as np
import pandas as pd
import math
from matplotlib.colors import ColorConverter

def plot_interest_rates(real, simulated):
  combined = pd.DataFrame()
  colors = []
  black = ColorConverter().to_rgba('black', alpha=0.3)
  for i, s in enumerate(simulated):
    combined = combined.join(s.dropna(), how='right', rsuffix=i)
    colors.append(black)
  combined = combined.join(real)
  colors.append('red')
  ax = combined.plot(color=colors, legend=False, ylim=(0, 25))
  ax.set_xlabel('Year')
  ax.set_ylabel('Interest rate')
  pylab.show()

def plot_results(results, binCount=None, xlabel='Return'):
  df = pd.DataFrame(results, columns=['value']).convert_objects(convert_numeric=True)
  plotRange = (min(results), max(results))
  plotLength = plotRange[1] - plotRange[0]
  if not binCount:
    binCount = int(math.ceil(len(results)/20))
  ax = df.plot(kind='hist', bins=binCount, color='gray', legend=False, use_index=False, range=plotRange)
  ax.set_ylabel('Count')
  ax.set_xlabel(xlabel)
  #step = int(math.ceil(plotLength / binCount))
  #ticks = [x for x in range(int(plotRange[0]), int(plotRange[1]), step)]
  #ax.set_xticks(ticks)
  pylab.show()