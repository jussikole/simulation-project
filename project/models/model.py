import numpy as np
import pandas as pd
import statsmodels.tsa.arima_model as arima

# Abstract class, create sub classes
class Model:
  def __init__(self):
    pass
    
  def estimate(self, rates):
    # Rates is a data frame with date and value columns
    pass
    
  def project(self, previous, date):
    # Return single value for the given date
    pass
    
  def __str__(self):
    return 'abstract model'

class ConstantModel(Model):
  def __init__(self):
    pass
    
  def estimate(self, rates):
    self.start = rates.tail(1).ix[0].real_rate
    
  def project(self, dates):
    return [self.start] * len(dates)
    
  def __str__(self):
    return 'Constant ' + str(self.start)
  
class NormalDistributionModel(Model):
  def __init__(self, drift=0):
    self.drift = drift
    self.mean = 1
    self.std = 0
    self.start = None
    
  def estimate(self, rates):
    diff = rates.diff(1)
    self.mean += diff.mean(0).real_rate
    self.std = diff.std(0).real_rate
    
  def project(self, dates):
    previous = self.start
    projections = []
    for date in dates:
      projection = previous * (self.mean + np.random.normal()*self.std)
      projections.append(projection)
      previous = projection
    # Return single value for the given date
    return projections
    
  def __str__(self):
    return "N model: mean={0:.6f} std={1:.2f}".format(self.mean, self.std)

class RevertingNormalModel(NormalDistributionModel):
  def __init__(self, reversionRate=1):
    self.reversionRate = reversionRate
    self.mean = 1
    self.std = 0
    self.start = None
    
  def estimate(self, rates):
    diff = rates.diff(1)
    self.mean = rates.real_rate.mean()
    self.std = diff.std(0).real_rate
    self.start = rates.tail(1).ix[0].real_rate
    
  def project(self, dates):
    previous = self.start
    mean = self.mean
    projections = []
    for date in dates:
      mean = (self.mean - previous) / self.reversionRate
      projection = previous * (1 + mean + np.random.normal()*self.std)
      projections.append(projection)
      previous = projection
    # Return single value for the given date
    return projections
    
  def __str__(self):
    return "N model: mean={0:.6f} std={1:.2f}".format(self.mean, self.std)
    
class ARMAModel(Model):
  def __init__(self, order):
    self.armaModel = None
    self.order = order
    
  def estimate(self, rates):
    self.armaModel = arima.ARMA(rates, self.order).fit()
    
  def project(self, dates):
    return self.armaModel.forecast(len(dates))[0]

  
    
  def __str__(self):
    return 'arma model'
    

